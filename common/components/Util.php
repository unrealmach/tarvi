<?php

namespace common\components;

use Yii;
use yii\base\Component;

/**
 * Utilitario para manejar metodos globales
 * @author Mauricio Chamorro
 */
class Util extends Component
{

    /**
     * Permite obtener el directorio de los assets publicos que maneja ese controlador
     * @author Mauricio Chamorro
     * @return String ruta del directorio publico
     */
    public static function getPublishAssetDirectory()
    {
        //obtiene el path del directorio para los assets publicados WEB
        list($path, $webPath) = Yii::$app->getAssetManager()->publish(\Yii::$app->controller->module->basePath
            .'/assets/js/'.Yii::$app->controller->id);
        return $webPath;
    }

    /**
     * Permite quitar el tiempo de un datetime
     * por defecto el date de salida es Y-m-d
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param String $datetime
     * @param String $format
     * @return String
     */
    public static function transformDateTimeToDate($datetime, $format = "Y-m-d")
    {
        $date = new \DateTime($datetime);
        return $date->format($format);
    }

    /**
     * Obtiene los dias de la semana segun una fecha de referencia
     * @author Mauricio Chamorro
     * @param String $fechaInicio si es null cogera el dia actual
     * @return array
     */
    public static function getDaysOfWeek($fechaInicio = null)
    {
        $days     = [];
        $start2   = is_null($fechaInicio) ? new \DateTime() : new \DateTime($fechaInicio);
        $end2     = is_null($fechaInicio) ? new \DateTime() : new \DateTime($fechaInicio);
        $start    = $start2->modify('this week');
        $end      = $end2->modify('this week +7 days');
        $interval = new \DateInterval('P1D'); //periodo por dia
        $period   = new \DatePeriod($start, $interval, $end);
        foreach ($period as $date2) {
            array_push($days, $date2->format('Y-m-d'));
        }
        return $days;
    }

    /**
     * Obtiene los dias del mes deacuedro a un dia de referencia
     * @author Mauricio CHamorro
     * @param String  $fechaInicio si es null trabajara con el dia actual
     * @return array
     */
    public static function getDaysOfMonth($fechaInicio = null)
    {
        $days     = [];
        $start    = is_null($fechaInicio) ? new \DateTime() : new \DateTime($fechaInicio);
        $start    = $start->modify('first day of this month');
        $end      = is_null($fechaInicio) ? new \DateTime() : new \DateTime($fechaInicio);
        $end      = $end->modify('last day of this month')->modify('+1 day');
        $interval = new \DateInterval('P1D'); //periodo por dia
        $period   = new \DatePeriod($start, $interval, $end);
        foreach ($period as $date3) {
            array_push($days, $date3->format('Y-m-d'));
        }
        return $days;
    }

    /**
     * obtiene las semanas del mes deacuerdo a una fecha de referencia
     * @author Mauricio Chamorro
     * @param String $fechaInicio si es null, trabajara con la fecha actual
     * @return array
     */
    public static function getWeeksOfMonth($fechaInicio = null)
    {
        $days  = [];
        $start = is_null($fechaInicio) ? new \DateTime() : new \DateTime($fechaInicio);
        $start = $start->modify('first day of this month');
        $end   = is_null($fechaInicio) ? new \DateTime() : new \DateTime($fechaInicio);
        $end   = $end->modify('last day of this month');

        $interval = new \DateInterval('P1W'); //periodo por dia
        $period   = new \DatePeriod($start, $interval, $end);
        foreach ($period as $date4) {
            $tempWeekIni = new \DateTime($date4->format('Y-m-d'));
            $tempWeekFin = new \DateTime($date4->format('Y-m-d'));
            array_push($days,
                ['inicioSemana' => $tempWeekIni->modify('this week')->format('Y-m-d'),
                'finSemana' => $tempWeekFin->modify('this week +6 days')->format('Y-m-d')]);
        }
        return $days;
    }

    /**
     * obtiene las fecha y hora
     * @author Luis Yaguapaz
     * @param none
     * @return datetime
     */
    public static function getFechaActual()
    {
        //$datetime = new \DateTime('now', new \DateTimeZone('UTC'));

        $tz_object = new \DateTimeZone('America/Guayaquil');
        $datetime  = new \DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime;
    }

    /**
     * Reemplaza la raya baja por un espacio
     * Sirve para mostrar en los labels o etiquetas en las vistas
     * @author Mauricio Chamorro
     * @param type $string
     * @return type
     */
    public static function reemplazarRayaBajaByEspacio($string)
    {
        return str_replace("_", " ", $string);
    }

    public static function getImages($primary_key)
    {
        $model = new \backend\modules\sitios\models\Atractivos();

        $ImageManagement = Yii::$app->ImageManagement;
        //obtiene las imagenes del modelo
        $atractivoModel  = $model->findOne($primary_key);

        $galeriaModel = $atractivoModel->getGaleria()->one();
        $imgs         = [$galeriaModel['galeria_logo'],
            $galeriaModel['galeria_uno'],
            $galeriaModel['galeria_dos'],
            $galeriaModel['galeria_tres']];


        //elimina todas las imagenes del cache
        //$ImageManagement->DeleteAllImagesOfModel($model, $primary_key);
        //enlaza el  grupo de imagenes al modelo
        //$ImageManagement->AttachImagesOfModel($model, $primary_key, $imgs);
        //obtiene todas las imagenes

        $images = $ImageManagement->GetAllsImageOfModel($model, $primary_key);
        //genera las imagenes con resolucion por defecto para el apk
        //para consumir con api
        $rutas  = $ImageManagement->GetImagesDefaultSize($images);
        $rutas  = $ImageManagement->GetImageBySizeOfModel($images,
            ['100x100', '400x200']);
        \yii\helpers\VarDumper::dump($rutas, 10, true);
        die();
        return $rutas;
    }

    /**
     * Transforma una string base64 a un fichero real para imagenes
     * @param type $strBase64
     * @param type $nombre
     * @return type
     */
    function base64ToImage($tipoentidad,$strBase64, $nombre)
    {
//        define('UPLOAD_DIR', 'images/');
//        $img     = $_POST['img'];
        $img     = str_replace('data:image/png;base64,', '', $strBase64);
        $img     = str_replace(' ', '+', $img);
        $data    = base64_decode($img);
        $file    = \Yii::getAlias('@frontend')."/web/images/qr/".$tipoentidad.$nombre.'.png';
        $success = file_put_contents($file, $data);
        return $nombre.".png";
    }

    /**
     * Da un string n base64 creado a partir del codigoqr
     * @param string $entidadTipo persona | empresa
     * @param string $codigo
     */
    function createQrGetString($entidadTipo, $codigo)
    {
        $path           = Yii::getAlias('@frontend')."/web".\Yii::$app->qrcode->create('[{"tipo":"'.$entidadTipo.'"},{"id":"'.$codigo.'"}]');
        $base64         = \Yii::$app->qrcode->getBase64($path);
        return  $base64;
    }
}