<?php

namespace common\components;

use Yii;
use yii\base\Component;

/**
 * Componente para el manejo de imagenes.
 * *Enlaza imagenes a modelos o tablas
 * *Cambio de tamaño
 * *Cortes
 * *Creación automatica de temporales con configuracion de medidas
 * *Muestra las imagenes por url
 * @before Se debe de tener declarado el comportamiento en el modelo
 * 'image' => [
  'class' => 'rico\yii2images\behaviors\ImageBehave',
  ]
 * @tutorial Se usa https://github.com/CostaRico/yii2-images
 * @author Mauricio Chamorro
 */
class ImageManagement extends Component
{
    /**
     * @var string[] Da las medidas por default de las imagenes que se presentan
     * en la apk
     */
    private static $_defaultSizesDisplayImg = [
        'pequenia' => '320x160',
        'mediana' => '420x210',
        'grande' => '600x300',
    ];

    /**
     * Enlaza a traves del modelo y el primary_key dado, las imagenes que se le
     * pasan el array de strings
     * @param \yii\db\ActiveRecord $modelSource
     * @param int $primary_key
     * @param string[] $urls Debe de ser la url con la cual se accede a
     * la imagen desde la web
     * @author Mauricio Chamorro
     */
    public static function AttachImagesOfModel($modelSource, $primary_key, $urls)
    {
        $model = $modelSource->findOne($primary_key);
        foreach ($urls as $key => $url) {
            $model->attachImage($url);
        }
    }

    /**
     * Quita todas las imagenes que se hayan enlazado al modelo, tanto en la
     * tabla img como de los temporales de cache
     * @param \yii\db\ActiveRecord $modelSource
     * @param int $primary_key
     * @author Mauricio Chamorro
     */
    public static function DeleteAllImagesOfModel($modelSource, $primary_key)
    {
        $model = $modelSource->findOne($primary_key);
        $model->removeImages();
    }

    /**
     * Da la imagen principal con la que se enlaza al modelo
     * @param \yii\db\ActiveRecord $modelSource
     * @param int $primary_key
     * @return string URL para la web que indica la ruta de la imagen
     * @author Mauricio Chamorro
     */
    public static function GetMainImageOfModel($modelSource, $primary_key)
    {
        $model = $modelSource->findOne($primary_key);
        return $model->getImage();
    }

    /**
     * Da todas las imagenes que se hayan enlazado al modelo
     * @param \yii\db\ActiveRecord $modelSource
     * @param int $primary_key
     * @return array|yii\db\ActiveRecord[] Array con modelos de la tabla img
     * @author Mauricio Chamorro
     */
    public static function GetAllsImageOfModel($modelSource, $primary_key)
    {
        $model = $modelSource->findOne($primary_key);
        return $model->getImages();
    }

    /**
     * Elimina una imagen de un modelo
     * @param \yii\db\ActiveRecord $modelSource De la tabla padre
     * @param int $primary_key
     * @param \yii\db\ActiveRecord $image_model De la tabla img
     * @author Mauricio Chamorro
     */
    public static function DeleteImageOfModel($modelSource, $primary_key,
                                              $image_model)
    {
        $model = $modelSource->findOne($primary_key);
        $model->removeImage($image_model);
    }

    /**
     * Cambia la imagen principal del modelo
     * @param \yii\db\ActiveRecord  $modelSource de la tabla padre
     * @param int $primary_key
     * @param string $absolutePathToImage
     * @author Mauricio Chamorro
     */
    public static function SetMainImage($modelSource, $primary_key,
                                        $absolutePathToImage)
    {
        $model = $modelSource->findOne($primary_key);

        $model->attachImage($absolutePathToImage, true); //will attach image and make it main
//TODO comprobar
//        foreach ($model->getImages() as $img) {
//            if ($img->id == $ourId) {
//                $model->setMainImage($img); //will set current image main
//            }
//        }
    }

    /**
     * Da las imagenes que estan definidas por defecto para el apk
     * @param \yii\db\ActiveRecord[] $imagesModel de la tabla img
     * @return type Description
     */
    public static function GetImagesDefaultSize($imagesModel)
    {
        $images = [];
        foreach ($imagesModel as $img) {
            foreach (self::$_defaultSizesDisplayImg as $key => $value) {
                array_push($images, $img->getUrl($value));
            }
        }

        return $images;
    }

    /**
     * Da las imagenes que se definan en el array sizes
     * @param \yii\db\ActiveRecord[] $imagesModel
     * @param string[] $sizes ['100x100']
     * @return array
     */
    public static function GetImageBySizeOfModel($imagesModel, $sizes)
    {

        $images = [];
        foreach ($imagesModel as $img) {
            foreach ($sizes as $key => $value) {
                array_push($images, $img->getUrl($value));
            }
        };

        return $images;
    }
}