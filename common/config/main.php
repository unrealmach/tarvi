<?php
return [
    'language' => 'es',
    'vendorPath' => dirname(dirname(__DIR__)).'/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'Util' => [
            'class' => 'common\components\Util'
        ],
        'qrcode' => [
            'class' => 'common\components\Yii2qrcode'
        ],
        'fileupload' => [
            'class' => 'yii\fileupload\Manager'
        ],
        'ImageManagement' => [
            'class' => 'common\components\ImageManagement'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
    ],
];
