<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    // require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php')
    //   require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'America/Lima',
    'modules' => [
        /**
         * @author Mauricio Chamorro <unrealmach@hotmail.com>
         */
        'v1' => [
            'basePath' => '@api/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
    //---------------------------------//
    ],
    'components' => [
//        'request' => [
//            'parsers' => [
//                'application/xml' => 'yii\web\XMLParser',
////                'application/json' => 'yii\web\JsonParser',
//            ]
//        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
        /**
         * @author Mauricio Chamorro <unrealmach@hotmail.com>
         */
             'rules' => [
                [
                    'pluralize' => false,
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/persona'],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                    // 'except' => ['delete', 'create', 'update', 'index'],
                    'extraPatterns' => [
                         'GET search' => 'search',
//                        'GET getProvincias' => 'getprovincias',
//                        'GET findProvincia' => 'getprovinciasbyname',
                    ],
                ],
                [
                    'pluralize' => false,
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/entidad'],
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                    // 'except' => ['delete', 'create', 'update', 'index'],
                    'extraPatterns' => [
                        'GET getlistaentidades' => 'getlistaentidades',
                        'GET getentidadqr' => 'getentidadqr',
                        'GET getentidadid' => 'getentidadid',
                    ],
                ],

            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
