<?php
return [
    'adminEmail' => 'admin@example.com',
    'restapi' => ['tokenExpire' => '10000'], //tiempo de expiracion desde el ultimo acceso de un usuario por rest api
];
