<?php

namespace api\modules\v1\models\activequeries;

/**
 * This is the ActiveQuery class for [[\api\modules\v1\models\TipoAtractivo]].
 *
 * @see \api\modules\v1\models\TipoAtractivo
 */
class TipoAtractivoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \api\modules\v1\models\TipoAtractivo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \api\modules\v1\models\TipoAtractivo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
