<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "{{%persona}}".
 *
 * @property integer $id_persona
 * @property string $codigo
 * @property string $fecha_registro
 * @property string $foto
 * @property string $nombre
 * @property string $apellido
 * @property string $direccion
 * @property string $telefono_fijo
 * @property string $telefono_movil
 * @property string $correo
 * @property string $profesion
 * @property string $cargo
 * @property string $pagina_web
 * @property string $empresa
 * @property string $logo
 * @property string $tarjeta
 *
 * @property Network[] $networks
 * @property Servicio[] $servicios
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%persona}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_registro'], 'safe'],
            [['codigo', 'foto', 'direccion', 'telefono_fijo', 'correo', 'profesion', 'cargo', 'pagina_web', 'empresa', 'logo', 'tarjeta'], 'string', 'max' => 100],
            [['nombre', 'apellido'], 'string', 'max' => 50],
            [['telefono_movil'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => Yii::t('app', 'Id Persona'),
            'codigo' => Yii::t('app', 'Codigo'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'foto' => Yii::t('app', 'Foto'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'direccion' => Yii::t('app', 'Direccion'),
            'telefono_fijo' => Yii::t('app', 'Telefono Fijo'),
            'telefono_movil' => Yii::t('app', 'Telefono Movil'),
            'correo' => Yii::t('app', 'Correo'),
            'profesion' => Yii::t('app', 'Profesion'),
            'cargo' => Yii::t('app', 'Cargo'),
            'pagina_web' => Yii::t('app', 'Pagina Web'),
            'empresa' => Yii::t('app', 'Empresa'),
            'logo' => Yii::t('app', 'Logo'),
            'tarjeta' => Yii::t('app', 'Tarjeta'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNetworks()
    {
        return $this->hasMany(Network::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @inheritdoc
     * @return \api\modules\v1\models\activequeries\PersonaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \api\modules\v1\models\activequeries\PersonaQuery(get_called_class());
    }
}
