<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Persona;
use backend\modules\rrhh\models\Servicio;
use backend\modules\rrhh\models\Network;
// use yii\web\BadRequestHttpException;
// use yii\helpers\ArrayHelper;
use Yii;
//use yii\rest\ActiveController;
use yii\web\Response;

class EntidadController extends \yii\web\Controller
{

    public function behaviors()
    {
        $behaviors                      = parent::behaviors();
        $behaviors['corsFilter']        = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['http://localhost:8100'],
                'Access-Control-Allow-Origin' => ['*'],
            ]
        ];
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    public function actionGetlistaentidades()
    {
        $request = Yii::$app->request;
        $param   = $request->get('query');
        $models  = Yii::$app->db
            ->createCommand("
                   SELECT * FROM (  (SELECT p.id_persona as id,"
                ." CASE p.logo WHEN '' THEN p.foto ELSE p.logo  END as img, "
                ."concat(p.nombre,' ', p.apellido) as nombres, "
                ."p.direccion, p.telefono_fijo, p.telefono_movil, "
                ."p.correo FROM persona p where p.nombre like :searchParam
                or p.apellido like :searchParam or p.profesion like :searchParam
                or p.cargo like :searchParam or p.empresa like :searchParam limit 15)
                
                UNION ALL

                (SELECT em.id_empresa as id,em.logo as img, em.nombre as nombres, em.direccion,
                em.telefono_fijo, em.telefono_movil, em.correo
                FROM `empresa` em WHERE
                em.nombre like :searchParam OR em.correo like :searchParam
                OR em.razon_social like :searchParam LIMIT 15 )) tmp order by tmp.nombres
                 ")
            ->bindValue(':searchParam', "%".$param."%")
            ->queryAll();

        if ($models) {
            return $models;
        } else {
            return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
        }
    }

    public function actionGetentidadqr()
    {
        $request = Yii::$app->request;
        $param   = $request->get('query');
        $tipo    = $request->get('tipo'); //empresa o persona
        switch ($tipo) {
            case 'persona':
                $models = Yii::$app->db
                    ->createCommand("SELECT p.id_persona as id,"
                        ." CASE p.logo WHEN '' THEN p.foto ELSE p.logo  END as img, "
                        ."concat(p.nombre,' ', p.apellido) as nombres, "
                        ."p.direccion, p.telefono_fijo, p.telefono_movil, "
                        ."p.correo FROM persona p where p.codigo = :searchParam ;")
                    ->bindValue(':searchParam', $param)
//                print_r($models->getRawSql());
//                die();
                    ->queryOne();
                break;
            case 'empresa':
                $models = Yii::$app->db
                    ->createCommand("SELECT em.id_empresa as id,em.logo as img,
                        em.nombre as nombres, em.direccion,
                em.telefono_fijo, em.telefono_movil, em.correo
                FROM `empresa` em where em.codigo = :searchParam ;")
                    ->bindValue(':searchParam', $param)
                    ->queryOne();
                break;
            default:
                //TODO lanzar excepcion o error
                break;
        }


        if ($models) {
            return $models;
        } else {
            return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
        }
    }

    public function actionGetentidadid()
    {
        $request = Yii::$app->request;
        $param   = $request->get('query');
        $tipo    = $request->get('tipo'); //empresa o persona
        switch ($tipo) {
            case 'persona':
                $modelEntidad  = Yii::$app->db
                    ->createCommand("SELECT * from persona where id_persona=:searchParam ")
                    ->bindValue(':searchParam', $param)
                    ->queryOne();
                $modelServicio = Servicio::find()->where(["id_persona" => $modelEntidad['id_persona']])->all();
                $modelNetwork  = Network::find()->where(["id_persona" => $modelEntidad['id_persona']])->all();
                break;
            case 'empresa':
                $modelEntidad  = Yii::$app->db
                    ->createCommand("SELECT * from empresa where id_empresa=:searchParam ")
                    ->bindValue(':searchParam', $param)
                    ->queryOne();
                $modelServicio = Servicio::find()->where(["id_empresa" => $modelEntidad['id_empresa']])->all();
                $modelNetwork  = Network::find()->where(["id_empresa" => $modelEntidad['id_empresa']])->all();
                break;
            default:
                break;
        }

        if ($modelEntidad) {
//            return $modelEntidad;
            return [
//                'd' => "ddd",
                'entidad' => $modelEntidad,
                'servicio' => $modelServicio,
                'network' => $modelNetwork
                ];
        } else {
            return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
        }
    }

//    public function actionGetentidadid()
//    {
//        $request = Yii::$app->request;
//        $param   = $request->get('query');
//        $tipo    = $request->get('tipo'); //empresa o persona
//        switch ($tipo) {
//            case 'persona':
//                $models = Yii::$app->db
//                    ->createCommand("SELECT * from persona where id_persona=:searchParam ")
//                    ->bindValue(':searchParam', $param)
////                print_r($models->getRawSql());
////                die();
//                    ->queryOne();
//                break;
//            case 'empresa':
//                $models = Yii::$app->db
//                     ->createCommand("SELECT * from empresa where id_empresa=:searchParam ")
//                    ->bindValue(':searchParam', $param)
//                    ->queryOne();
//                break;
//            default:
//                //TODO lanzar excepcion o error
//                break;
//        }
//
//
//        if ($models) {
//            return $models;
//        } else {
//            return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
//        }
//    }
}