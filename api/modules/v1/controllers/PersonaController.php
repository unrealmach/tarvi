<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Persona;
// use yii\web\BadRequestHttpException;
// use yii\helpers\ArrayHelper;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;

class PersonaController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Persona';

    public function behaviors()
    {
        #$behaviors = parent::behaviors();
        #CORS
        $behaviors                      = parent::behaviors();
        $behaviors['corsFilter']        = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrict access to
                'Origin' => ['http://localhost:8100'],
                'Access-Control-Allow-Origin' => ['*'],
//                'Access-Control-Request-Method' => ['POST', 'PUT'],
            // Allow only POST and PUT methods
//                'Access-Control-Request-Headers' => ['X-Wsse'],
            // Allow only headers 'X-Wsse'
//                'Access-Control-Allow-Credentials' => true,
            // Allow OPTIONS caching
//                'Access-Control-Max-Age' => 3600,
            // Allow the X-Pagination-Current-Page header to be exposed to the browser.
//                'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
            ]
        ];
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
//                'text/html' => Response::FORMAT_JSON,
                'application/json' => Response::FORMAT_JSON,
//                'application/xml' => Response::FORMAT_XML,
            ],
        ];

        return $behaviors;
    }

    /**
     *   Método que Busca atractivo turistico 
     *   @param string $atractivo
     *   @author Luis Yaguapaz <luis.yaguapaz@gmail.com>
     *   @return List<Atractivos> listado de las Atractivos
     *   @error {code:String, err:String}
     *   @http_request_method => GET
     */
    public function actionGetatractivobyname()
    {
        $request = Yii::$app->request;
        if ($request->get('atractivo')) {
            $param  = $request->get('atractivo');
            $models = Yii::$app->db->createCommand("
      SELECT
            atractivos.atractivo_id,
            atractivos.atractivo_nombre,
            atractivos.atractivo_descripcion,
            tipo_atractivo.tipo_atractivo_nombre,
            
            image.urlAlias as galeria_logo,
            canton.canton_nombre,
            provincia.provincia_nombre
            FROM
            atractivos
            INNER JOIN contadores ON contadores.atractivo_id = atractivos.atractivo_id
            INNER JOIN tipo_atractivo ON atractivos.tipo_atractivo_id = tipo_atractivo.tipo_atractivo_id
            INNER JOIN canton ON atractivos.canton_id = canton.canton_id
            INNER JOIN galeria ON atractivos.galeria_id = galeria.galeria_id
            INNER JOIN provincia ON canton.provincia_id = provincia.provincia_id,
            image
            WHERE
            atractivos.canton_id = canton.canton_id AND
            atractivos.galeria_id = galeria.galeria_id AND
            atractivos.atractivo_id = contadores.atractivo_id AND
            image.isMain =1 and
            canton.provincia_id = provincia.provincia_id AND atractivos.atractivo_nombre like '%".$param."%'
            or atractivos.atractivo_descripcion like '%".$param."%' or canton.canton_nombre like '%".$param."%'
            ORDER BY
            contadores.contador_visita DESC
            LIMIT 20
      ")->queryAll();

            if ($models) {
                return $models;
            } else {
                return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
            }
        } else {
            return array('err' => 'Los datos enviados no son correctos o invalidos.',
                'code' => 'ERROR_DATOS_ENVIADOS');
        }
    }

    /**
     *   Método que Busca atractivo por idCanton 
     *   @param integer $canton
     *   @author Luis Yaguapaz <luis.yaguapaz@gmail.com>
     *   @return List<Atractivos> listado de las Atractivos
     *   @error {code:String, err:String}
     *   @http_request_method => GET
     */
    public function actionGetatractivobycantonid()
    {
        $request = Yii::$app->request;
        if ($request->get('canton')) {
            $param  = $request->get('canton');
            $models = Yii::$app->db->createCommand("
      SELECT
          atractivos.atractivo_id,
          atractivos.atractivo_nombre,
          tipo_atractivo.tipo_atractivo_nombre,
          galeria.galeria_logo,
          canton.canton_nombre,
          provincia.provincia_nombre,
image.urlAlias as portada
          FROM
          atractivos
          INNER JOIN contadores ON contadores.atractivo_id = atractivos.atractivo_id
          INNER JOIN tipo_atractivo ON atractivos.tipo_atractivo_id = tipo_atractivo.tipo_atractivo_id
          INNER JOIN canton ON atractivos.canton_id = canton.canton_id
          INNER JOIN galeria ON atractivos.galeria_id = galeria.galeria_id
          INNER JOIN provincia ON canton.provincia_id = provincia.provincia_id,
       image
          WHERE
          atractivos.canton_id = ".$param." AND
          atractivos.galeria_id = galeria.galeria_id AND
          atractivos.atractivo_id = contadores.atractivo_id AND
          canton.provincia_id = provincia.provincia_id AND
              atractivos.atractivo_id = image.itemId
              and image.isMain =1
          ORDER BY
          contadores.contador_visita DESC
          LIMIT 20
      ")->queryAll();

            if ($models) {
                return $models;
            } else {
                return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
            }
        } else {
            return array('err' => 'Los datos enviados no son correctos o invalidos.',
                'code' => 'ERROR_DATOS_ENVIADOS');
        }
    }

    /**
     *   Método que Busca atractivo por idAtractivo 
     *   @param integer $canton
     *   @author Luis Yaguapaz <luis.yaguapaz@gmail.com>
     *   @return List<Atractivos> listado de las Atractivos
     *   @error {code:String, err:String}
     *   @http_request_method => GET
     */
    public function actionGetatractivobyid()
    {

        $request = Yii::$app->request;
        if ($request->get('id')) {
            $param  = $request->get('id');
            $models = Yii::$app->db->createCommand("
      SELECT
          atractivos.atractivo_id,
          atractivos.atractivo_nombre,
          atractivos.atractivo_direccion,
          atractivos.atractivo_descripcion,
          atractivos.atractivo_horario_atencion,
          atractivos.atractivo_telefono,
          atractivos.atractivo_sitio_web,
          atractivos.atractivo_propietario,
          atractivos.atractivo_correo,
          atractivos.atractivo_latitud,
          atractivos.atractivo_longitud,
          atractivos.atractivo_estado,
          tipo_atractivo.tipo_atractivo_nombre,
          contadores.contador_visita,
          canton.canton_nombre,
          provincia.provincia_nombre
          FROM
          atractivos
          INNER JOIN contadores ON contadores.atractivo_id = atractivos.atractivo_id
          INNER JOIN tipo_atractivo ON atractivos.tipo_atractivo_id = tipo_atractivo.tipo_atractivo_id
          INNER JOIN canton ON atractivos.canton_id = canton.canton_id
          INNER JOIN provincia ON canton.provincia_id = provincia.provincia_id
          WHERE
          atractivos.canton_id = canton.canton_id AND
          
          atractivos.atractivo_id = contadores.atractivo_id AND
          canton.provincia_id = provincia.provincia_id AND
          atractivos.atractivo_id = ".$param."
          ORDER BY
          contadores.contador_visita DESC
          LIMIT 20
      ")->queryAll();

            if ($models) {
                return $models;
            } else {
                return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
            }
        } else {
            return array('err' => 'Los datos enviados no son correctos o invalidos.',
                'code' => 'ERROR_DATOS_ENVIADOS');
        }
    }

    /**
     *   Método que Busca atractivo por idTipoAtractivo 
     *   @param integer $canton
     *   @author Luis Yaguapaz <luis.yaguapaz@gmail.com>
     *   @return List<Atractivos> listado de las Atractivos
     *   @error {code:String, err:String}
     *   @http_request_method => GET
     */
    public function actionGetatractivobytipo()
    {
        $request = Yii::$app->request;
        $models;
        // if ($request->get('tipo')&& $request->get('dimesion')) {
        if ($request->get('tipo')) {
            $param = $request->get('tipo');

            if ($param != 7) {
                $models = Yii::$app->db->createCommand("
              SELECT
              atractivos.atractivo_id,
              atractivos.atractivo_nombre,
              canton.canton_nombre,
              provincia.provincia_nombre,
              image.urlAlias as portada
              FROM
                          atractivos
                          INNER JOIN contadores ON contadores.atractivo_id = atractivos.atractivo_id
                          INNER JOIN tipo_atractivo ON atractivos.tipo_atractivo_id = tipo_atractivo.tipo_atractivo_id
                          INNER JOIN canton ON atractivos.canton_id = canton.canton_id

                          INNER JOIN provincia ON canton.provincia_id = provincia.provincia_id,
                          image
              WHERE
              atractivos.canton_id = canton.canton_id AND
              atractivos.atractivo_id = contadores.atractivo_id AND
              canton.provincia_id = provincia.provincia_id AND
              atractivos.tipo_atractivo_id = ".$param." AND
              atractivos.atractivo_id = image.itemId
              and image.isMain =1
              ORDER BY
                          contadores.contador_visita DESC
              LIMIT 20
                    ")->queryAll();
            } else {
                $models = Yii::$app->db->createCommand("
              SELECT
                atractivos.atractivo_id,
                atractivos.atractivo_nombre,
                canton.canton_nombre,
                provincia.provincia_nombre,
                atractivos.atractivo_latitud,
                atractivos.atractivo_longitud
                FROM
                atractivos
                INNER JOIN contadores ON contadores.atractivo_id = atractivos.atractivo_id
                INNER JOIN tipo_atractivo ON atractivos.tipo_atractivo_id = tipo_atractivo.tipo_atractivo_id
                INNER JOIN canton ON atractivos.canton_id = canton.canton_id
                INNER JOIN provincia ON canton.provincia_id = provincia.provincia_id
                WHERE
                atractivos.canton_id = canton.canton_id AND
                atractivos.atractivo_id = contadores.atractivo_id AND
                canton.provincia_id = provincia.provincia_id AND
                atractivos.tipo_atractivo_id = ".$param."
                ORDER BY contadores.contador_visita DESC
                LIMIT 20")->queryAll();
            }

            if ($models) {
                // \yii\helpers\VarDumper::dump($models, 10, true);
                //   die();
                return $models;
            } else {
                return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
            }
        } else {
            return array('err' => 'Los datos enviados no son correctos o invalidos.',
                'code' => 'ERROR_DATOS_ENVIADOS');
        }
    }

    /**
     *   Método que obtiene las imagenes de un atractivo turistico 
     *   @param integer $key (atractivo_id) , String $size (tamaño) , integer limit (>1 para enviar todas las imgagenes)
     *   @author Luis Yaguapaz <luis.yaguapaz@gmail.com>
     *   @return List<Atractivos> listado de las Atractivos
     *   @error {code:String, err:String}
     *   @http_request_method => GET
     */
    public function actionGetimages()
    {

        $request = Yii::$app->request;

        if ($request->get('key') && $request->get('size') && $request->get('limit')) {
            $ruta = [];

            $models;
            $primary_key  = $request->get('key');
            $sizeRequest  = $request->get('size');
            $limit        = $request->get('limit');
            $queryPartOne = "/admin/yii2images/images/image-by-item-and-alias?item=Atractivos".$primary_key."&dirtyAlias=";
            if ($limit > 1) {
                // $models = Yii::$app->db->createCommand("select CONCAT('" . $queryPartOne . "',urlAlias ,'_" . $sizeRequest . ".') as galeria_logo from image where  itemId=" . $primary_key )->queryAll();
                $models    = Yii::$app->db->createCommand("select itemId as atractivo_id, CONCAT('".$queryPartOne."',urlAlias ,'_".$sizeRequest.".') as galeria_logo from image where  itemId=".$primary_key)->queryAll();
                // \yii\helpers\VarDumper::dump($models, 10, true);
                // die();
                $cont      = 0;
                $resultado = "";
                foreach ($models as $key) {

                    if ($cont == 0) {
                        $galeria_logo = $key['galeria_logo'];
                        // array_push($ruta, array( 'galeria_logo' => $key['galeria_logo']));
                    }
                    if ($cont == 1) {
                        $galeria_logo1 = $key['galeria_logo'];
                        // array_push($ruta, array( 'galeria_logo1' => $key['galeria_logo']));
                    }
                    if ($cont == 2) {
                        $galeria_logo2 = $key['galeria_logo'];
                        // array_push($ruta, array( 'galeria_logo2' => $key['galeria_logo']));
                    }
                    if ($cont == 3) {
                        $galeria_logo3 = $key['galeria_logo'];
                        // array_push($ruta, array('galeria_logo3' => $key['galeria_logo']));
                    }
                    $cont++;
                    // array_push($ruta,  $resultado);
                }
                array_push($ruta,
                    array('galeria_logo' => $galeria_logo, 'galeria_logo1' => $galeria_logo1,
                    'galeria_logo2' => $galeria_logo2, 'galeria_logo3' => $galeria_logo3));
            } else {
                $models = Yii::$app->db->createCommand("select itemId as atractivo_id,CONCAT('".$queryPartOne."',urlAlias ,'_".$sizeRequest.".') as galeria_logo from image where  itemId=".$primary_key." and isMain = 1")->queryAll();

                foreach ($models as $key) {
                    array_push($ruta,
                        array('atractivo_id' => $key['atractivo_id'], 'galeria_logo' => $key['galeria_logo']));
                }
            }
            if ($ruta) {
                return $ruta;
            } else {
                return array('err' => 'NO SE ENCONTRARON COINCIDENCIAS.', 'code' => 'ERROR_NO_DATA_FOUND');
            }
        } else {
            return array('err' => 'Los datos enviados no son correctos o invalidos.',
                'code' => 'ERROR_DATOS_ENVIADOS');
        }
    }
}