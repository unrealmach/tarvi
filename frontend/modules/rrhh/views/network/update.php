<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Network */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Network',
]) . $model->id_network;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Networks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_network, 'url' => ['view', 'id' => $model->id_network]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="network-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
