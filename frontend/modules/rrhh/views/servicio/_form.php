<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\rrhh\models\Persona;
use backend\modules\rrhh\models\Empresa;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Servicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'id_empresa',
        ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->widget(Select2::classname(),
        [
        'data' => ArrayHelper::map(Empresa::find()->where('user_id = "'.Yii::$app->user->id.'"')->all(),
            'id_empresa', 'nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccione Empresa', 'class' => 'col-sm-2',],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label(null, ['class' => 'col-sm-3 control-label']);
    ?>
    <?php
    echo $form->field($model, 'id_persona',
        ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->widget(Select2::classname(),
        [
        'data' => ArrayHelper::map(Persona::find()->where('user_id = "'.Yii::$app->user->id.'"')->all(),
            'id_persona', 'nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccione Persona', 'class' => 'col-sm-2',],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ])->label(null, ['class' => 'col-sm-3 control-label']);
    ?>

    <?=
        $form->field($model, 'nombre',
            [
            'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese...'])
        ->label(null, ['class' => 'col-sm-3 control-label'])
    ?>
    <?=
        $form->field($model, 'descripcion',
            [
            'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
        ->textarea(['rows' => 6, 'placeholder' => 'Ingrese...'])
        ->label(null, ['class' => 'col-sm-3 control-label'])
    ?>


    <div class="form-group">
        <?=
        Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app',
                    'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
