<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Servicio */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Servicio',
]) . $model->id_servicio;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Servicios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_servicio, 'url' => ['view', 'id' => $model->id_servicio]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="servicio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
