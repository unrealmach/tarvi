<?php

use yii\helpers\Url;
?>
<div class="rrhh_front-default-index">
    <div class="jumbotron">
        <p class="lead">Proceda a crear una tarjeta </p>
        <hr class="my-4">
        <p>Tiene dos opciones: personal y empresarial</p>
    </div>
    <div class="row">

        <div class="col-sm-6 initial-card" >
            <div class="jumbotron">
                <h1 class="display-3">Personal</h1>
                <p class="lead">Especialmente creada para una tarjeta personalizada personal</p>
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="<?php
                    echo Url::to([
                        'persona/create']);
                    ?>" role="button">Siguiente</a>
                </p>
            </div>
        </div>
        <div class="col-sm-6 initial-card" >
            <div class="jumbotron">
                <h1 class="display-3">Empresarial</h1>
                <p class="lead">Especialmente creada para una tarjeta personalizada empresarial</p>
                <p class="lead">
                    <a class="btn btn-primary btn-lg" href="<?php
                       echo Url::to([
                           'empresa/create']);
                       ?>" role="button">Siguiente</a>
                </p>
            </div>
        </div>
    </div>
</div>
