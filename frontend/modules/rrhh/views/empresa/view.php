<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Empresa */

$this->title                   = $model->id_empresa;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Empresas'), 'url' => [
        'index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'),
            ['update', 'id' => $model->id_empresa],
            ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_empresa],
            [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app',
                    'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
        <?=
        Html::a(Yii::t('app', 'Ir a la lista'), ['index',],
            ['class' => 'btn btn-info'])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_empresa',
            'codigo',
                [
                'attribute' => 'foto',
                'value' => "/".$model->logo,
                'format' => ['image', ['width' => '200', 'height' => '200']],
            ],
            'nombre',
            'direccion',
            'telefono_fijo',
            'telefono_movil',
            'correo',
            'pagina_web',
            'razon_social:ntext',
                [
                'attribute' => 'qr_code',
                'value' => "/images/qr/".'empresa'.$model->id_empresa.".png",
                'format' => ['image', ['width' => '200', 'height' => '200']],
            ],
        ],
    ])
    ?>

</div>
