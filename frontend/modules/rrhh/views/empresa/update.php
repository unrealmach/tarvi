<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Empresa */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Empresa',
]) . $model->id_empresa;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Empresas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_empresa, 'url' => ['view', 'id' => $model->id_empresa]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="empresa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
