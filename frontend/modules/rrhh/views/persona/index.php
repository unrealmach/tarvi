<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\rrhh\models\PersonaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Personas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persona-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);    ?>

    <p>
        <?=
        Html::a(Yii::t('app', 'Create Persona'), ['create'],
            ['class' => 'btn btn-success'])
        ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'codigo',
            'nombre',
            'apellido',
//            'direccion',
//            'telefono_fijo',
//            'telefono_movil',
            'correo',
            // 'profesion',
            // 'cargo',
            // 'pagina_web',
            // 'empresa',
            // 'logo',
            // 'tarjeta',
            // 'qr_code:ntext',
            [
                'attribute' => 'qr_code',
                'value' => function ($model) {
                    return "/images/qr/".'persona'.$model->id_persona.".png";
                },
                'format' => ['image', ['width' => '200', 'height' => '200']],
            ],
                ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
