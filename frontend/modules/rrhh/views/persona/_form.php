<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Persona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persona-form">

    <?php
    $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'id' => $model->formName(),
    ]);
    ?>
    <?php
    echo $form->field($model, 'codigo',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'fecha_registro',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    $rutaImagenVoltaje = Yii::getAlias("/frontend/web/").$model["foto"];
    echo $form->field($model, "tmpFoto",
        [
        'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(FileInput::classname(),
        [
        'options' => [
            'multiple' => false,
            'accept' => 'image/*',
            'class' => 'optionvalue-img'
        ],
        'pluginOptions' => [
            'previewFileType' => 'image',
            'showCaption' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-default btn-sm ',
            'browseLabel' => ' Subir imagen',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i>',
            'removeClass' => 'btn btn-danger btn-sm hide',
            'removeIcon' => '<i class="fa fa-trash"></i>',
            'previewSettings' => [
                'image' => ['width' => '140px', 'height' => 'auto']
            ],
            'initialPreview' => [
                Html::img($rutaImagenVoltaje,
                    ['width' => '140px', 'height' => 'auto', 'class' => 'file-preview-image ',
                    'alt' => 'Logo', 'title' => 'Logo']),
            ],
        ]
    ])->label(null, ['class' => 'col-sm-2 control-label']);
    ?>

    <?php
    echo $form->field($model, 'nombre',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'apellido',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'direccion',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'telefono_fijo',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'telefono_movil',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'correo',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'profesion',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'cargo',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'pagina_web',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'empresa',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'logo',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->widget(FileInput::classname(),
            [
            'options' => ['accept' => 'image/*'],
        ])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>
    <?php
    echo $form->field($model, 'tarjeta',
            ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
        ->textInput(['maxlength' => true, 'placeholder' => 'Ingrese'])
        ->label(null, ['class' => 'col-sm-2 control-label']);
    ?>




    <div class="form-group">
        <?=
        Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app',
                    'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
