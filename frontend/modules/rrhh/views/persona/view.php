<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\rrhh\models\Persona */

$this->title                   = $model->id_persona;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personas'), 'url' => [
        'index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persona-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=
        Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_persona],
            ['class' => 'btn btn-primary'])
        ?>
        <?=
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id_persona],
            [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app',
                    'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
        <?=
        Html::a(Yii::t('app', 'Ir a la lista'), ['index',],
            ['class' => 'btn btn-info'])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo',
            'fecha_registro',
                [
                'attribute' => 'foto',
                'value' => "/".$model->foto,
                'format' => ['image', ['width' => '200', 'height' => '200']],
            ],
//            'foto',
            'nombre',
            'apellido',
            'direccion',
            'telefono_fijo',
            'telefono_movil',
            'correo',
            'profesion',
            'cargo',
            'pagina_web',
            'empresa',
                [
                'attribute' => 'foto',
                'value' => "/".$model->logo,
                'format' => ['image', ['width' => '200', 'height' => '200']],
            ],
//            'logo',
            'tarjeta',
                [
                'attribute' => 'qr_code',
                'value' => "/images/qr/".'persona'.$model->id_persona.".png",
                'format' => ['image', ['width' => '200', 'height' => '200']],
            ],
        ],
    ])
    ?>

</div>
