<?php

namespace frontend\modules\rrhh\controllers;

use Yii;
use backend\modules\rrhh\models\Persona;
use backend\modules\rrhh\models\PersonaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PersonaController implements the CRUD actions for Persona model.
 */
class PersonaController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Persona models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new PersonaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',
                [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Persona model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view',
                [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Persona model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Persona();
        $model->getImagesFiles();

        $model->fecha_registro = date('Y-m-d H:i:s');
        if ($model->load(Yii::$app->request->post())) {

            $bool = $model->uploadImage();
//            var_dump($bool);
            if ($bool !== false) {
                foreach ($bool as $key => $value) {
                    if (!is_null($value)) {
                        if ($key == 0) {
                            $bool[0]->saveAs($model->foto);
                        } else if ($key == 1) {
                            $bool[1]->saveAs($model->logo);
                        }
                    }
                }
            }

            $model->qr_code = \Yii::$app->Util->createQrGetString('persona',
                $model->codigo);
            $model->user_id = Yii::$app->user->id;
            if ($model->save()) {
                Yii::$app->Util->base64ToImage('persona', $model->qr_code,
                    $model->id_persona);
                return $this->redirect(['view', 'id' => $model->id_persona]);
            }
        }
        return $this->render('create',
                [
                'model' => $model,
        ]);
    }

    /**
     * Updates an existing Persona model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->getImagesFiles();
        if ($model->load(Yii::$app->request->post())) {

            $bool = $model->uploadImage();
//            var_dump($bool);
            if ($bool !== false) {
                foreach ($bool as $key => $value) {
                    if (!is_null($value)) {
                        if ($key == 0) {
                            $bool[0]->saveAs($model->foto);
                        } else if ($key == 1) {
                            $bool[1]->saveAs($model->logo);
                        }
                    }
                }
            }
            $model->qr_code = \Yii::$app->Util->createQrGetString('persona',
                $model->codigo);
            if ($model->save()) {
                Yii::$app->Util->base64ToImage('persona', $model->qr_code,
                    $model->id_persona);
                return $this->redirect(['view', 'id' => $model->id_persona]);
            }
        }
        return $this->render('update',
                [
                'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Persona model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Persona model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Persona the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Persona::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}