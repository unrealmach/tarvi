<?php

namespace frontend\modules\rrhh;

/**
 * rrhh_front module definition class
 */
class RecursosHumanos extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\rrhh\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
