<?php

namespace frontend\modules\rbac\models\activequeries;

/**
 * This is the ActiveQuery class for [[\frontend\modules\rbac\models\AuthAssignment]].
 *
 * @see \frontend\modules\rbac\models\AuthAssignment
 */
class AuthAssignmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \frontend\modules\rbac\models\AuthAssignment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \frontend\modules\rbac\models\AuthAssignment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
