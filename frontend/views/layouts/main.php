<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body background='<?php Yii::getAlias('@frontend') ?>/images/background.jpg'>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Tarvi',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems[] =
                    ['label' => 'Contact', 'url' => ['/site/contact']];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Home', 'url' => ['/site/index']];
//                $menuItems[] = ['label' => 'About', 'url' => ['/site/about']];
                $menuItems[] = ['label' => 'Signup', 'url' => ['/user/registration/register ']];
                $menuItems[] = ['label' => 'Login', 'url' => ['/user/security/login']];
            } else {
                $menuItems[] = ['label' => 'Tarjetas', 'url' => ['/rrhh_front']];
                $menuItems[] = ['label' => 'Servicios', 'url' => ['/rrhh_front/servicio']];
                $menuItems[] = ['label' => 'Network', 'url' => ['/rrhh_front/network']];
                $menuItems[] = '<li>'
                    .Html::beginForm(['/user/security/logout'], 'post')
                    .Html::submitButton(
                        'Logout ('.Yii::$app->user->identity->username.')',
                        ['class' => 'btn btn-link logout']
                    )
                    .Html::endForm()
                    .'</li>';
            }
            
            
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

            <div class="container">
<?=
Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs']
            : [],
])
?>
                <?= Alert::widget() ?>
                <div class="body-container">
                <?= $content ?>
                </div>

            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <p class="pull-right"><?= Yii::powered() ?></p>
            </div>
        </footer>

<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
