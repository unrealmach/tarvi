<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'zdu617xUWfJ0LNW7yAD5Qjg_2AitHtpb',
        ],
    ],
];

$config['components']['assetManager']['forceCopy'] = true; // borrar en produccion
if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];

    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                //  'class' => 'common\generators\crud\Generator',
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => ['adminlte_crud' => '@common/generators/crud/adminlte_crud'],
            ]
        ],
    ];
}

return $config;
