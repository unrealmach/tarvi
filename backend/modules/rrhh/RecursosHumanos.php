<?php

namespace backend\modules\rrhh;

/**
 * rrhh_back module definition class
 */
class RecursosHumanos extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\rrhh\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
