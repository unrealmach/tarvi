<?php

namespace backend\modules\rrhh\models;

use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "{{%persona}}".
 *
 * @property integer $id_persona
 * @property string $codigo
 * @property string $fecha_registro
 * @property string $foto
 * @property string $nombre
 * @property string $apellido
 * @property string $direccion
 * @property string $telefono_fijo
 * @property string $telefono_movil
 * @property string $correo
 * @property string $profesion
 * @property string $cargo
 * @property string $pagina_web
 * @property string $empresa
 * @property string $logo
 * @property string $tarjeta
 * @property string $qr_code
 * @property integer $user_id
 *
 * @property Network[] $networks
 * @property Servicio[] $servicios
 */
class Persona extends \yii\db\ActiveRecord
{
    public $tmpFoto;
    public $tmpLogo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%persona}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['user_id','fecha_registro', 'tmpFoto', 'tmpLogo'], 'safe'],
                [['qr_code'], 'string'],
                [['codigo', 'tmpFoto', 'direccion', 'telefono_fijo', 'correo', 'profesion',
                'cargo', 'pagina_web', 'empresa', 'logo', 'tarjeta'], 'string', 'max' => 100],
            [['nombre', 'apellido', 'telefono_movil', 'codigo', 'foto', 'direccion',
                'telefono_fijo', 'correo', 'profesion',
                'cargo',], 'required'],
                [['nombre', 'apellido'], 'string', 'max' => 50],
                [['telefono_movil'], 'string', 'max' => 15],
                [['tmpFoto', 'tmpLogo'], 'file', 'extensions' => 'jpg, png,jpeg,bmp'],
            [['tmpFoto', 'tmpLogo'], 'image', 'maxWidth' => 500, 'maxHeight' => 500, 'extensions' => 'jpg, png,jpeg,bmp', 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_persona' => Yii::t('app', 'Persona'),
            'codigo' => Yii::t('app', 'Código'),
            'fecha_registro' => Yii::t('app', 'Fecha Registro'),
            'foto' => Yii::t('app', 'Foto'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'direccion' => Yii::t('app', 'Dirección'),
            'telefono_fijo' => Yii::t('app', 'Teléfono Fijo'),
            'telefono_movil' => Yii::t('app', 'Teléfono Movil'),
            'correo' => Yii::t('app', 'Correo'),
            'profesion' => Yii::t('app', 'Profesión'),
            'cargo' => Yii::t('app', 'Cargo'),
            'pagina_web' => Yii::t('app', 'Página Web'),
            'empresa' => Yii::t('app', 'Empresa'),
            'logo' => Yii::t('app', 'Logo'),
            'tarjeta' => Yii::t('app', 'Tarjeta'),
            'qr_code' => Yii::t('app', 'Qr Code'),
            'tmpFoto' => Yii::t('app', 'Foto'),
            'tmpLogo' => Yii::t('app', 'Logo'),
            'user_id' => Yii::t('app', 'Usuario'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNetworks()
    {
        return $this->hasMany(Network::className(),
                ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(),
                ['id_persona' => 'id_persona']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\rrhh\models\activequeries\PersonaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\rrhh\models\activequeries\PersonaQuery(get_called_class());
    }

    /**
     * Para cuando se actualize o cree una nueva imagen
     * @author Mauricio Chamorro
     * @return type
     */
    public function getImagesFiles()
    {
        return [isset($this->foto) ? $this->foto : $this->foto='images/default_persona.png',
            isset($this->logo) ? $this->logo : $this->logo='images/default_empresa.png'];
    }

    /**
     * Instancia un nuevo objeto para subir las imagenes
     * @author Mauricio Chamorro
     * @return boolean
     */
    public function uploadImage()
    {
        $fecha = time();

        $file  = UploadedFile::getInstance($this, 'tmpFoto');
        $file2 = UploadedFile::getInstance($this, 'tmpLogo');

        $imageName  = trim($this->codigo.'_persona_'.$fecha);
        $imageName2 = trim($this->codigo.'_persona_'.$fecha);

        if (!empty($file) && empty($file2)) { 
            $file->name = 'uploads/foto/'.$imageName."_".$file->name; // override the file name
            $this->foto = $file;
            return [$file, null];
        } else if (!empty($file) && !empty($file2)) { // las dos no vacias
            $file->name  = 'uploads/foto/'.$imageName."_".$file->name; // override the file name
            $file2->name = 'uploads/logo/'.$imageName2."_".$file2->name; // override the file name

            $this->foto       = $file;
            $this->logo = $file2;
            return [$file, $file2];
        }
        return false;
    }
}