<?php

namespace backend\modules\rrhh\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\rrhh\models\Servicio;

/**
 * ServicioSearch represents the model behind the search form about `backend\modules\rrhh\models\Servicio`.
 */
class ServicioSearch extends Servicio
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['id_servicio', 'id_empresa', 'id_persona'], 'integer'],
                [['nombre', 'descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query1 = (new \yii\db\Query())
            ->select("servicio.*")
            ->from('servicio')
            ->innerJoin('persona', ' persona.id_persona = servicio.id_persona ')
            ->innerJoin('user', ' user.id = persona.user_id ')
            ->where(["persona.user_id"=> Yii::$app->user->id])
//            ->andFilterWhere(['like', 'servicio.nombre', $this->nombre])
//            ->andFilterWhere(['like', 'servicio.descripcion', $this->descripcion])
//            ->params([':user_id',Yii::$app->user->id])
//            ->limit(10)
        ;
//        var_dump($query1->all());
//        die();

        $query2 = (new \yii\db\Query())
            ->select("servicio.*")
            ->from('servicio')
            ->innerJoin('empresa', ' empresa.id_empresa = servicio.id_empresa ')
            ->innerJoin('user', ' user.id = empresa.user_id ')
//            ->where("empresa.user_id=1")
//            ->andFilterWhere(['like', 'servicio.nombre', $this->nombre])
//            ->andFilterWhere(['like', 'servicio.descripcion', $this->descripcion])
            ->where(["empresa.user_id"=> Yii::$app->user->id]);

        $query1->union($query2, true); //false is UNION, true is UNION ALL
        $sql   = $query1->createCommand()->getRawSql();
        $sql   .= ' ORDER BY nombre ASC';
        $query = Servicio::findBySql($sql);




        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'id_servicio' => $this->id_servicio,
//            'id_empresa' => $this->id_empresa,
//            'id_persona' => $this->id_persona,
//        ]);
        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}