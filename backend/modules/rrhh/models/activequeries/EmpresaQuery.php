<?php

namespace backend\modules\rrhh\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\rrhh\models\Empresa]].
 *
 * @see \backend\modules\rrhh\models\Empresa
 */
class EmpresaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\rrhh\models\Empresa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\rrhh\models\Empresa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
