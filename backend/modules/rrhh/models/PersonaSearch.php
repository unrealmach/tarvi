<?php

namespace backend\modules\rrhh\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\rrhh\models\Persona;

/**
 * PersonaSearch represents the model behind the search form about `backend\modules\rrhh\models\Persona`.
 */
class PersonaSearch extends Persona
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['id_persona','user_id'], 'integer'],
                [['codigo','user_id', 'fecha_registro', 'foto', 'nombre', 'apellido', 'direccion',
                'telefono_fijo', 'telefono_movil', 'correo', 'profesion', 'cargo',
                'pagina_web', 'empresa', 'logo', 'tarjeta', 'qr_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Persona::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_persona' => $this->id_persona,
            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere([
            'user_id' => Yii::$app->user->id,
//            'fecha_registro' => $this->fecha_registro,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'telefono_fijo', $this->telefono_fijo])
            ->andFilterWhere(['like', 'telefono_movil', $this->telefono_movil])
            ->andFilterWhere(['like', 'correo', $this->correo])
            ->andFilterWhere(['like', 'profesion', $this->profesion])
            ->andFilterWhere(['like', 'cargo', $this->cargo])
            ->andFilterWhere(['like', 'pagina_web', $this->pagina_web])
            ->andFilterWhere(['like', 'empresa', $this->empresa])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'tarjeta', $this->tarjeta])
            ->andFilterWhere(['like', 'qr_code', $this->qr_code]);

        return $dataProvider;
    }
}