<?php

namespace backend\modules\rrhh\models;

use Yii;

/**
 * This is the model class for table "{{%servicio}}".
 *
 * @property integer $id_servicio
 * @property integer $id_empresa
 * @property integer $id_persona
 * @property string $nombre
 * @property string $descripcion
 *
 * @property Empresa $idEmpresa
 * @property Persona $idPersona
 */
class Servicio extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%servicio}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['id_empresa', 'id_persona'], 'validateOnlyOne'],
                [['id_empresa', 'id_persona'], 'integer'],
                [['descripcion'], 'string'],
                [['descripcion', 'nombre'], 'required'],
                [['nombre'], 'string', 'max' => 100],
                [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(),
                'targetAttribute' => ['id_empresa' => 'id_empresa']],
                [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(),
                'targetAttribute' => ['id_persona' => 'id_persona']],
        ];
    }

    /**
     * Solo para la creacion
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function validateOnlyOne($attribute, $params, $validator)
    {
        if ($this->id_empresa != "" && $this->id_persona != "") {
            $this->addError($attribute,
                'Seleccione solo una empresa o una persona a la vez');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_servicio' => Yii::t('app', 'Id Servicio'),
            'id_empresa' => Yii::t('app', 'Empresa'),
            'id_persona' => Yii::t('app', 'Persona'),
            'nombre' => Yii::t('app', 'Nombre'),
            'descripcion' => Yii::t('app', 'Descripción'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(),
                ['id_empresa' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(),
                ['id_persona' => 'id_persona']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\rrhh\models\activequeries\ServicioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\rrhh\models\activequeries\ServicioQuery(get_called_class());
    }

//    public static function getServicios($user_id)
//    {
//        $query1 = (new \yii\db\Query())
//            ->select("servicio.*")
//            ->from('servicio')
//            ->innerJoin('persona', ' persona.id_persona = servicio.id_persona ')
//            ->innerJoin('user', ' user.id = persona.user_id ')
//            ->where(["persona.user_id" => $user_id]);
//        $query2 = (new \yii\db\Query())
//            ->select("servicio.*")
//            ->from('servicio')
//            ->innerJoin('empresa', ' empresa.id_empresa = servicio.id_empresa ')
//            ->innerJoin('user', ' user.id = empresa.user_id ')
//            ->where(["empresa.user_id" => $user_id]);
//        $query1->union($query2, true); //false is UNION, true is UNION ALL
//        $sql    = $query1->createCommand()->getRawSql();
//        $sql    .= ' ORDER BY nombre ASC';
//        $query  = Servicio::findBySql($sql);
//        print_r($query);
//        die();
//        return $query->all();;
//    }
}