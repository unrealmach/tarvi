<?php

namespace backend\modules\rrhh\models;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "{{%empresa}}".
 *
 * @property integer $id_empresa
 * @property string $codigo
 * @property string $logo
 * @property string $nombre
 * @property string $direccion
 * @property string $telefono_fijo
 * @property string $telefono_movil
 * @property string $correo
 * @property string $pagina_web
 * @property string $razon_social
 * @property string $tarjeta
 * @property string $qr_code
 * @property int $user_id
 *
 * @property Network[] $networks
 * @property Servicio[] $servicios
 */
class Empresa extends \yii\db\ActiveRecord
{
    public $tmpLogo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%empresa}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['user_id', 'tmpLogo'], 'safe'],
                [['razon_social', 'qr_code'], 'string'],
                [['logo', 'nombre', 'direccion', 'telefono_fijo', 'telefono_movil',
                'correo'], 'required'],
                [['codigo', 'logo','tmpLogo', 'nombre', 'direccion', 'telefono_fijo', 'telefono_movil',
                'correo', 'pagina_web', 'tarjeta'], 'string', 'max' => 100],
                [['tmpLogo'], 'file', 'extensions' => 'jpg, png,jpeg,bmp'],
                [['tmpLogo'], 'image', 'maxWidth' => 500, 'maxHeight' => 500,
                'extensions' => 'jpg, png,jpeg,bmp', 'maxSize' => 1024 * 1024 * 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_empresa' => Yii::t('app', 'Id Empresa'),
            'codigo' => Yii::t('app', 'Código'),
            'logo' => Yii::t('app', 'Logo'),
            'nombre' => Yii::t('app', 'Nombre'),
            'direccion' => Yii::t('app', 'Dirección'),
            'telefono_fijo' => Yii::t('app', 'Teléfono Fijo'),
            'telefono_movil' => Yii::t('app', 'Teléfono Movil'),
            'correo' => Yii::t('app', 'Correo'),
            'pagina_web' => Yii::t('app', 'Página Web'),
            'razon_social' => Yii::t('app', 'Razón Social'),
            'tarjeta' => Yii::t('app', 'Tarjeta'),
            'qr_code' => Yii::t('app', 'Qr Code'),
            'tmpLogo' => Yii::t('app', 'Logo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNetworks()
    {
        return $this->hasMany(Network::className(),
                ['id_empresa' => 'id_empresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(),
                ['id_empresa' => 'id_empresa']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\rrhh\models\activequeries\EmpresaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\rrhh\models\activequeries\EmpresaQuery(get_called_class());
    }

    /**
     * Para cuando se actualize o cree una nueva imagen
     * @author Mauricio Chamorro
     * @return type
     */
    public function getImagesFiles()
    {
        return [isset($this->logo) ? $this->logo : $this->logo = 'images/default_empresa.png'];
    }

    /**
     * Instancia un nuevo objeto para subir las imagenes
     * @author Mauricio Chamorro
     * @return boolean
     */
    public function uploadImage()
    {
        $fecha = time();

        $file2 = UploadedFile::getInstance($this, 'tmpLogo');


        $imageName2 = trim($this->codigo.'_empresa_'.$fecha);

        if (!empty($file2)) { // las dos no vacias
            $file2->name                    = 'uploads/logo/'.$imageName2."_".$file2->name; // override the file name
            $this->logo = $file2;
            return [$file2];
        }
        return false;
    }
}