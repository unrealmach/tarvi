<?php

namespace backend\modules\rrhh\models;

use Yii;

/**
 * This is the model class for table "{{%network}}".
 *
 * @property integer $id_network
 * @property integer $id_persona
 * @property integer $id_empresa
 * @property string $nombre
 * @property string $link
 *
 * @property Persona $idPersona
 * @property Empresa $idEmpresa
 */
class Network extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%network}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_persona', 'id_empresa'], 'integer'],
            [['id_empresa', 'id_persona'], 'validateOnlyOne'],
            [['nombre','link'],'required'],
            [['nombre'], 'string', 'max' => 50],
            [['link'], 'string', 'max' => 255],
            [['id_persona'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['id_persona' => 'id_persona']],
            [['id_empresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['id_empresa' => 'id_empresa']],
        ];
    }

     /**
     * Solo para la creacion
     * @param type $attribute
     * @param type $params
     * @param type $validator
     */
    public function validateOnlyOne($attribute, $params, $validator)
    {
         if($this->id_empresa=="" && $this->id_persona==""){
             $this->addError($attribute,
                 'Seleccione solo una empresa o una persona a la vez');
        }
        if($this->id_empresa!="" &&$this->id_persona!=""){
             $this->addError($attribute,
                'Seleccione solo una empresa o una persona a la vez');
        }
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_network' => Yii::t('app', 'Network'),
            'id_persona' => Yii::t('app', 'Persona'),
            'id_empresa' => Yii::t('app', 'Empresa'),
            'nombre' => Yii::t('app', 'Nombre'),
            'link' => Yii::t('app', 'Link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPersona()
    {
        return $this->hasOne(Persona::className(), ['id_persona' => 'id_persona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresa()
    {
        return $this->hasOne(Empresa::className(), ['id_empresa' => 'id_empresa']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\rrhh\models\activequeries\NetworkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\rrhh\models\activequeries\NetworkQuery(get_called_class());
    }

//    public function getNetworks($params)
//    {
//        $query1 = (new \yii\db\Query())
//            ->select("network.*")
//            ->from('network')
//            ->innerJoin('persona',' persona.id_persona = network.id_persona ')
//            ->innerJoin('user',' user.id = persona.user_id ')
//            ->where(["persona.user_id"=> Yii::$app->user->id]);
//        $query2 = (new \yii\db\Query())
//            ->select("network.*")
//            ->from('network')
//            ->innerJoin('empresa',' empresa.id_empresa = network.id_empresa ')
//            ->innerJoin('user',' user.id = empresa.user_id ')
//            ->where(["empresa.user_id"=>Yii::$app->user->id]);
//        return $dataProvider;
//    }
}
