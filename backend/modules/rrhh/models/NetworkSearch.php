<?php

namespace backend\modules\rrhh\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\rrhh\models\Network;

/**
 * NetworkSearch represents the model behind the search form about `backend\modules\rrhh\models\Network`.
 */
class NetworkSearch extends Network
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_network', 'id_persona', 'id_empresa'], 'integer'],
            [['nombre', 'link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query1 = (new \yii\db\Query())
            ->select("network.*")
            ->from('network')
            ->innerJoin('persona',' persona.id_persona = network.id_persona ')
            ->innerJoin('user',' user.id = persona.user_id ')
            ->where(["persona.user_id"=> Yii::$app->user->id]);
//             ->andFilterWhere(['like', 'servicio.nombre', $this->nombre])
//            ->andFilterWhere(['like', 'servicio.link', $this->link])
//            ->params([':user_id',Yii::$app->user->id])
//            ->limit(10)
//        var_dump($query1->all());
//        die();

        $query2 = (new \yii\db\Query())
            ->select("network.*")
            ->from('network')
            ->innerJoin('empresa',' empresa.id_empresa = network.id_empresa ')
            ->innerJoin('user',' user.id = empresa.user_id ')
            ->where(["empresa.user_id"=>Yii::$app->user->id]);
//            ->andFilterWhere(['like', 'servicio.nombre', $this->nombre])
//            ->andFilterWhere(['like', 'servicio.link', $this->link])
//            ->params([':user_id',Yii::$app->user->id])
//            ->limit(10)

        $query1->union($query2, true); //false is UNION, true is UNION ALL
        $sql   = $query1->createCommand()->getRawSql();


        $sql   .= ' ORDER BY nombre ASC';
        $query = Network::findBySql($sql);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id_network' => $this->id_network,
//            'id_persona' => $this->id_persona,
//            'id_empresa' => $this->id_empresa,
//        ]);
//
//        $query->andFilterWhere(['like', 'nombre', $this->nombre])
//            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
