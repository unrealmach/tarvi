<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">


                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
//            [
//                'options' => ['class' => 'sidebar-menu'],
//                'items' => [
//                    ['label' => 'Menu Principal', 'options' => ['class' => 'header']],
//                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],
//                      'visible' => Yii::$app->user->can('administrador'),
//                    ],
//                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],
//                     'visible' => Yii::$app->user->can('administrador'),
//                    ],
//                    ['label' => 'User Managment', 'icon' => 'fa fa-user', 'url' => ['/user/admin'],
//                     'visible' => Yii::$app->user->can('administrador'),
//                    ],
//                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
//                    [
//                        'label' => 'Ubicaciones',
//                        'icon' => 'fa fa-map-marker',
//                        'url' => '#',
//                         'visible' => Yii::$app->user->can('administrador'),
//                        'items' => [
//                            ['label' => 'Provincias', 'icon' => 'fa fa-indent', 'url' => ['/ubicaciones/provincia'],],
//                            ['label' => 'Añadir Provincia', 'icon' => 'fa fa-plus', 'url' => ['/ubicaciones/provincia/create'],],
//                            ['label' => 'Cantones', 'icon' => 'fa fa-indent', 'url' => ['/ubicaciones/canton'],],
//                            ['label' => 'Añadir Canton', 'icon' => 'fa fa-plus', 'url' => ['/ubicaciones/canton/create'],],
//
//                        ],
//                    ],
//                    [
//                        'label' => 'Planes',
//                        'icon' => 'fa fa-area-chart',
//                        'url' => '#',
//                         'visible' => Yii::$app->user->can('administrador'),
//                        'items' => [
//                            ['label' => 'Planes', 'icon' => 'fa fa-indent', 'url' => ['/planes/planes'],],
//                            ['label' => 'Añadir Plan', 'icon' => 'fa fa-plus', 'url' => ['/planes/planes/create'],],
//                            ['label' => 'Plan de Sitio', 'icon' => 'fa fa-indent', 'url' => ['/planes/planeshasatractivos'],],
//                            ['label' => 'Añadir un Plan Sitio', 'icon' => 'fa fa-map-signs', 'url' => ['/planes/planeshasatractivos/create'],],
//
//
//                        ],
//                    ],
//                    [
//                        'label' => 'Sitios',
//                        'icon' => 'fa fa-home',
//                        'url' => '#',
//                         'visible' => Yii::$app->user->can('colaborador'),
//                        'items' => [
//                            ['label' => 'Sitios', 'icon' => 'fa fa-indent', 'url' => ['/sitios/atractivos'],],
//                            ['label' => 'Añadir Sitio', 'icon' => 'fa fa-cart-plus', 'url' => ['/sitios/atractivos/create'],],
//                            ['label' => 'Eventos', 'icon' => 'fa fa-indent', 'url' => ['/sitios/evento'],],
//                            ['label' => 'Añadir Evento', 'icon' => 'fa fa-cart-plus', 'url' => ['/sitios/evento/create'],],
//
//                        ],
//                    ],
//                    [
//                        'label' => 'Sitios',
//                        'icon' => 'fa fa-home',
//                        'url' => '#',
//                         'visible' => Yii::$app->user->can('administrador'),
//                        'items' => [
//                            ['label' => 'Sitios', 'icon' => 'fa fa-indent', 'url' => ['/sitios/atractivos'],],
//                            ['label' => 'Añadir Sitio', 'icon' => 'fa fa-cart-plus', 'url' => ['/sitios/atractivos/create'],],
//                            ['label' => 'Eventos', 'icon' => 'fa fa-indent', 'url' => ['/sitios/evento'],],
//                            ['label' => 'Añadir Evento', 'icon' => 'fa fa-cart-plus', 'url' => ['/sitios/evento/create'],],
//                            ['label' => 'Tipo Sitios', 'icon' => 'fa fa-indent', 'url' => ['/sitios/tipoatractivo'],],
//                            ['label' => 'Añadir Tipo Sitios', 'icon' => 'fa fa-map-signs', 'url' => ['/sitios/tipoatractivo/create'],],
//
//
//                        ],
//                    ],
//                    [
//                        'label' => 'Contadores',
//                        'icon' => 'fa fa-bar-chart',
//                        'url' => ['/sitios/contadores'],
//                         'visible' => Yii::$app->user->can('administrador'),
//
//                    ],
//                    // [
//                    //     'label' => 'Same tools',
//                    //     'icon' => 'fa fa-share',
//                    //     'url' => '#',
//                    //     'items' => [
//                    //         ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
//                    //         ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
//                    //         [
//                    //             'label' => 'Level One',
//                    //             'icon' => 'fa fa-circle-o',
//                    //             'url' => '#',
//                    //             'items' => [
//                    //                 ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                    //                 [
//                    //                     'label' => 'Level Two',
//                    //                     'icon' => 'fa fa-circle-o',
//                    //                     'url' => '#',
//                    //                     'items' => [
//                    //                         ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                    //                         ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
//                    //                     ],
//                    //                 ],
//                    //             ],
//                    //         ],
//                    //     ],
//                    // ],
//                ],
//            ]
        ) ?>

    </section>

</aside>
